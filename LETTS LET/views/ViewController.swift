//
//  ViewController.swift
//  Task Manager
//
//  Created by a3331 on 1/19/19.
//  Copyright © 2019 TechTeamAfrica. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
class ViewController: UIViewController {

    @IBOutlet weak var txtpassword: UITextField!
    @IBOutlet weak var txtemail: UITextField!
    @IBOutlet weak var signupoutlet: UIButton!
    @IBOutlet weak var loginbtnoutlet: UIButton!
    
    var uid :String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
       self.loginbtnoutlet.layer.cornerRadius = 5.0
        self.signupoutlet.layer.cornerRadius = 5.0
        
       // loginAsUser()
    }


    @IBAction func btnLogin(_ sender: Any) {
        if txtemail.text != nil && txtpassword.text != nil{
            
            Auth.auth().signIn(withEmail: txtemail.text!, password: txtpassword.text!){(result,error) in
                if error != nil{
                    print("erro")
                    
                    let rr = error
                    print(rr!)
                    self.showSimpleAlert(msg: "wrong Email or password, please check and try again")
                }else{
                    print("logged in")
                    self.uid = (result?.user.uid)!
                    
                   self.performSegue(withIdentifier: "loginvc", sender: self)
//                    let ref = Database.database().reference(withPath: "users").child(uid!)
//                    ref.setValue(["email":self.txtemail.text!])
//                    ref.setValue(["password":self.txtpassword.text!])
                }
                
            }
        }else{
            self.showSimpleAlert(msg: "Email or password can not be empty")
        }
    }
    
   
    /**
     error login Alert
     - Show alert with title and alert message and basic two actions
     */
    func showSimpleAlert( msg:String) {
        let alert = UIAlertController(title: "Error", message: msg,         preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { _ in
            //Cancel Action
        }))
      
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    func loginAsUser()  {
        Auth.auth().signIn(withEmail: "mustaphaadebayo560@yahoo.com", password: "08172999948"){(result,error) in
            if error != nil{
                print("erro")
            }else{
                print("logged in")
                self.uid = (result?.user.uid)!
                
                self.performSegue(withIdentifier: "loginvc", sender: self)
                //                    let ref = Database.database().reference(withPath: "users").child(uid!)
                //                    ref.setValue(["email":self.txtemail.text!])
                //                    ref.setValue(["password":self.txtpassword.text!])
            }
            
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "loginvc"{
            let taskvc = segue.destination as! AlltaskVC
            taskvc.userID = uid
           
        }
        
//        let navigation = segue.destination as! UINavigationController
//        let taskvc = navigation.topViewController as! AlltaskVC
//        taskvc.userID = uid
        
        
    }
    
    
    
    
    @IBAction func btnAgent(_ sender: Any) {
        
        //agentSegue
        
        self.performSegue(withIdentifier: "agentSegue", sender: self)

    }
    
}

