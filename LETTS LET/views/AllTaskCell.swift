//
//  AllTaskCell.swift
//  Task Manager
//
//  Created by a3331 on 1/20/19.
//  Copyright © 2019 TechTeamAfrica. All rights reserved.
//

import UIKit

class AllTaskCell: UITableViewCell {

    @IBOutlet weak var imgChecked: UIImageView!
    @IBOutlet weak var viewTask: UIView!
    @IBOutlet weak var tasklbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewTask.layer.cornerRadius = 4.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
