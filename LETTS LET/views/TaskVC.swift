//
//  TaskVC.swift
//  Task Manager
//
//  Created by a3331 on 1/20/19.
//  Copyright © 2019 TechTeamAfrica. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth

struct Tasks1 {
    var isChecked :Bool
    var taskName: String
}
class TaskVC: UIViewController {
    var taskArr:[Tasks]=[]
    @IBOutlet weak var btnlogOutlet: UIButton!
    
    var userID:String?
var num = 0
    
    
  //var taskArr:[10]=[]
    var yourOtherArray = ["MonkeysRule", "RemoveMe", "SwiftRules"]
    //var arr=[Any]()
    var acceptedOrRejected:Int?
    @IBOutlet weak var lblTask: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
//acceptedOrRejected = 0
       // labelUpadet()
        
        btnlogOutlet.layer.cornerRadius = 5.0
        lblTask.layer.cornerRadius = 2.0
        loadTasks()
       // setWelcomeName()
        if let uid = userID{
           // welcomeLabel.text = uid
        }
        
        num = taskArr.count
        
        let gesture = UIPanGestureRecognizer(target: self, action: #selector(wasDragged(gestureRecognizer:)))
        lblTask.addGestureRecognizer(gesture)
        
        // Do any additional setup after loading the view.
        
        
    }
    
    
    
    func loadTasks()  {
        
        let ref = Database.database().reference(withPath: "users").child(userID!).child("Tasks")
        ref.observeSingleEvent(of: .value) { (snapshot) in
            for child in snapshot.children.allObjects as! [DataSnapshot]{
                let taskname = child.key
                
                let taskref = ref.child(taskname)
                taskref.observeSingleEvent(of: .value, with: { (tasksnapshot) in
                    let value = tasksnapshot.value as? NSDictionary
                    let isChecked = value!["isChecked"] as? Bool
                    self.taskArr.append(Tasks(isChecked: isChecked!, taskName: taskname))
                   // self.acceptedOrRejected =  child
                  //  self.arr.append(contentsOf: self.taskArr)
                    self.num = self.taskArr.count
                   // print(self.taskArr[self.acceptedOrRejected!].taskName)
                   // self.tblTask.reloadData()
                })
            }
        }
        
        
       
       // print(acceptedOrRejected as Any)
       
    }
    
    
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    
    @objc func wasDragged(gestureRecognizer: UIPanGestureRecognizer) {
        let labelPoint = gestureRecognizer.translation(in: view)
        lblTask.center = CGPoint(x: view.bounds.width / 2 + labelPoint.x, y: view.bounds.height / 2 + labelPoint.y)
        
        let xFromCenter = view.bounds.width / 2 - lblTask.center.x
        
        var rotation = CGAffineTransform(rotationAngle: xFromCenter / 200)
        
        let scale = min(100 / abs(xFromCenter), 1)
        
        var scaledAndRotated = rotation.scaledBy(x: scale, y: scale)
        
        lblTask.transform = scaledAndRotated
        
        if gestureRecognizer.state == .ended {
            
            
            
            if lblTask.center.x < (view.bounds.width / 2 - 100) {
                print("Not Interested")
               
                
                
                
                num -= 1
                
                let ref = Database.database().reference(withPath: "users").child(userID!).child("Tasks").child(taskArr[num].taskName)
                
               
//                print(num)
//                print(taskArr[num].taskName)
                if num < 0 {
                     lblTask.text = "No more Task"
                }else{
                   lblTask.text = taskArr[num].taskName
                    
                    if taskArr[num].isChecked{
                        taskArr[num].isChecked = false
                        
                        ref.updateChildValues(["isChecked": false])
                    }
                    
                    // acceptedOrRejected! -= 1 //"rejected"
                }
                
                
            }
            if lblTask.center.x > (view.bounds.width / 2 + 100) {
                print("Interested")
                num -= 1
//                print(num)
                     let ref = Database.database().reference(withPath: "users").child(userID!).child("Tasks").child(taskArr[num].taskName)
                
                
                
//               // print(taskArr[num].taskName)
             //  acceptedOrRejected! -= 1 //"accepted"
                if num < 0 {
                    lblTask.text = "No more Task"
                }else{
                    lblTask.text = taskArr[num].taskName
                    if taskArr[num].isChecked{
                        taskArr[num].isChecked = true
                        
                        ref.updateChildValues(["isChecked": true])
                    }
                    
                     //acceptedOrRejected! -= 1 //"rejected"
                }
            }
            
//            if acceptedOrRejected != "" && displayUserID != "" {
//                PFUser.current()?.addUniqueObject(displayUserID, forKey: acceptedOrRejected)
//
//                PFUser.current()?.saveInBackground(block: { (success, error) in
//                    if success {
//                        self.updateImage()
//                    }
//                })
//            }
            
            rotation = CGAffineTransform(rotationAngle: 0)
            
            scaledAndRotated = rotation.scaledBy(x: 1, y: 1)
            
            lblTask.transform = scaledAndRotated
            
            lblTask.center = CGPoint(x: view.bounds.width / 2, y: view.bounds.height / 2)
        }
    }
    
    @IBAction func btnLogout(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        
    }
    
    func labelUpadet()  {
        
        lblTask.text = yourOtherArray[acceptedOrRejected!]
    }
    
    
    @IBAction func btnPending(_ sender: Any) {
        
        
      
     
        
        
        
    }
}
